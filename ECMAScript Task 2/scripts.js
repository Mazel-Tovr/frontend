
const table = document.getElementById("tableStatement") //getting our table

var checkedCheckboxCount = 0;
noneGroup();

function fillTable(array) {
    titleInit();
    for (let index = 0; index < array.length; index++) {
        let row = document.createElement("TR");
        let td1 = document.createElement("TD");
        let td2 = document.createElement("TD");
        let td3 = document.createElement("TD");
        let td4 = document.createElement("TD");
        let td5 = document.createElement("TD");

        var date = new Date(array[index].date);

        row.id = array[index]._id;

        td1.innerHTML = date.toLocaleDateString('ru-RU');
        td2.innerHTML = date.toLocaleTimeString('ru-RU');
        td3.innerHTML = array[index].type;

        if (array[index].amount > 0) {
            td4.innerHTML = array[index].amount.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB' });
            td4.style.color = "#37760C";
        }
        else {
            td5.innerHTML = array[index].amount.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB' });
            td5.style.color = "#FF0000";
        }

        row.appendChild(td1);
        row.appendChild(td2);
        row.appendChild(td3);
        row.appendChild(td4);
        row.appendChild(td5);
        table.appendChild(row);

    }
}

function groupByDate(array) {
    clearTable();

    let cells = table.getElementsByTagName("td"); //get our table cells
    let row = document.createElement("TR");
    for (let index = 0; index < 2; index++) {
        row.appendChild(document.createElement("TD"));
    }
    table.appendChild(row);
    cells[0].innerHTML = "Дата";
    cells[1].innerHTML = "Счет";

    for (let index = 0; index < array.length; index++) {
        let row = document.createElement("TR");
        let td1 = document.createElement("TD");
        let td2 = document.createElement("TD");


        var date = new Date(array[index].date);

        row.id = array[index]._id;

        td1.innerHTML = date.toLocaleDateString('ru-RU');


        if (array[index].amount > 0) {
            td2.innerHTML = array[index].amount.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB' });
            td2.style.color = "#37760C";
        }
        else {
            td2.innerHTML = array[index].amount.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB' });
            td2.style.color = "#FF0000";
        }

        row.appendChild(td1);
        row.appendChild(td2);
        table.appendChild(row);
    }
}

function titleInit() {
    let cells = table.getElementsByTagName("td"); //get our table cells
    let row = document.createElement("TR");
    for (let index = 0; index < 5; index++) {
        row.appendChild(document.createElement("TD"));
    }
    table.appendChild(row);
    cells[0].innerHTML = "Дата";
    cells[1].innerHTML = "Время";
    cells[2].innerHTML = "Тип";
    cells[3].innerHTML = "Приход";
    cells[4].innerHTML = "Расход";
}

function noneGroup() {
    clearTable();
    fillTable(sortArray(-1, myStatementData));
}


function sortArray(order, array_) {
    let array = array_.slice(0).reverse();//get cope 
    array.sort((rowA, rowB) => new Date(rowA.date) > new Date(rowB.date) ? (1 * order) : (-1 * order));
    return array;
}

function clearTable() {
    while (table.hasChildNodes()) {
        table.removeChild(table.lastChild);
    }
}

function showCheckBox(flag) {
    let td1 = document.getElementById('date-box');
    let td2 = document.getElementById('time-box');
    let td3 = document.getElementById('type-box');
    let td4 = document.getElementById('income-box');
    let td5 = document.getElementById('costs-box');
    if (flag) {
        td1.style.display = "inline-block"; td1.checked = false;
        td2.style.display = "inline-block"; td2.checked = false;
        td3.style.display = "inline-block"; td3.checked = false;
        td4.style.display = "inline-block"; td4.checked = false;
        td5.style.display = "inline-block"; td5.checked = false;
        checkedCheckboxCount = 0;
    }
    else {
        td1.style.display = "none";
        td2.style.display = "none";
        td3.style.display = "none";
        td4.style.display = "none";
        td5.style.display = "none";
    }
}

function change() {
    if (document.getElementById("group-type-select").options.selectedIndex == 0) {
        noneGroup();
        showCheckBox(true)
    } else {
        showCheckBox(false)
        groupByDate(sortArray(-1, getRidOfDublicateDate(myStatementData)));
    }
}

function onChangeBox(value, name) {
    let cell = table.getElementsByTagName("tr");
    if (document.getElementById(name).checked) {
        checkedCheckboxCount++;
        //hide 
        if (checkedCheckboxCount < 5) {
            for (let index = 0; index < cell.length; index++) {

                cell[index].cells[value].style.display = "none";
            }
        }
        else {
            document.getElementById(name).checked = false
            checkedCheckboxCount--;
        }
    } else {
        //show back
        for (let index = 0; index < cell.length; index++) {

            cell[index].cells[value].style.display = "table-cell";

        }
        checkedCheckboxCount--;
    }
}
function getRidOfDublicateDate(array_) {
   
    let array = array_.slice(0).reverse();//get array cope
   
    let groupedStatementData = array.reduce((acc, n) => {
        let obj = acc.find(
            m => new Date(m.date).toLocaleDateString('ru-RU') === new Date(n.date).toLocaleDateString('ru-RU') );
        if (!obj) {
            acc.push(obj = { _id: n._id, date: n.date, type: n.type, amount: 0 });
        }
        obj.amount += n.amount;
        return acc;
    }, []);

    return groupedStatementData;
}
